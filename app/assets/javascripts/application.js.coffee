#= require jquery
#= require jquery_ujs
#= require twitter/bootstrap
#= require bootstrap
#= require jquery.maskedinput
#= require validations
#= require turbolinks
@initMask = () ->
  if $(".phone").length
    $(".phone").mask "+7 (999) 999-9999"

$(document)
  .on 'page:change', ->
    initMask()


