#= require jquery
#= require jquery_ujs
#= require twitter/bootstrap
#= require bootstrap
#= require helpers/alerts
#= require validations
#= require jquery.maskedinput
#= require turbolinks

@initMask = () ->
  if $(".phone").length
    $(".phone").mask "+7 (999) 999-9999"

$(document)
  .on 'page:change', ->
    return
  .on 'click', '.js-modal', (e)->
    e.preventDefault()
    $target = $($(@).attr('href'))
    $target.modal('show')

