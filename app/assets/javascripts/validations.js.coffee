#= require jquery.validate
#= require jquery.validate.additional-methods
#= require jquery.validate.localization/messages_ru
$(document)
  .on 'page:change', ->
    $('#new_user').validate

      rules:
        'user[phone]':
          minlength: 17
      messages:
        'user[phone]':
          minlength: 'Введите корректный номер телефона'


      errorElement: "span"
      errorClass: "text-danger"
      errorPlacement: (error, element) ->
        if element.parent(".input-group").length
          error.insertAfter element.parent()
        else
          if element.parent("label").length
            error.insertAfter element.parent("label")
          else
            if element.parent(".checkbox-wrapper").length
              error.insertAfter element.parent(".checkbox-wrapper")
            else
              error.insertAfter element
        return

    $('.grant_form').validate
      errorElement: "span"
      errorClass: "text-danger"
      errorPlacement: (error, element) ->
        if element.parent('label').length
          element.next('span').css 'border', '1px solid red'
        else
          element.css 'border', '1px solid red'
        return
