#= require jquery
#= require jquery_ujs
# require twitter/bootstrap
# require bootstrap
# require ckeditor/init
#= require welcome/carousel
#= require welcome/scroll
#= require welcome/slider
#= require welcome/wow
#= require validations
#= require jquery.maskedinput
#= require turbolinks

@initMask = () ->
  if $(".phone").length
    $(".phone").mask "+7 (999) 999-9999"

$(document).on 'page:change', ->
  initMask()

  wow = new WOW(
    boxClass: 'wow'
    animateClass: 'animated'
    offset: 0
    mobile: false
    live: true
    callback: (box) ->
  )
  wow.init()
  $('.home-header-bottom').on 'click', 'a', (event) ->
    event.preventDefault()
    id = $(this).attr('href')
    top = $(id).offset().top
    $('body,html').animate { scrollTop: top }, 1000
