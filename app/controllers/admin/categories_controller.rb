class Admin::CategoriesController < AdminController
  defaults resource_class: Category, collection_name: 'categories', instance_name: 'category'

  def create
    create! do |format|
      format.html { redirect_to admin_posts_path }
    end
  end

  def update
    update! do |format|
      format.html { redirect_to admin_posts_path }
    end
  end

  def destroy
    destroy! do |format|
      format.html { redirect_to admin_posts_path }
    end
  end

  protected

  def collection
    get_collection_ivar ||
      set_collection_ivar(end_of_association_chain.paginate page: params[:page])
  end

  def category_params
    params.require(:category).permit(:id, :name, :is_public)
  end
end
