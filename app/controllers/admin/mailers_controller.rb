class Admin::MailersController < AdminController

  def index
    @settings = Setting.unscoped.mailers
  end

  def edit
    @setting = Setting.unscoped.find params[:id]
  end

  def update
    setting = Setting.unscoped.find params[:id]
    respond_to do |format|
      Setting[setting.var] = params[:setting][:value]
      format.html { redirect_to admin_mailers_path, notice: 'Успешное редактирование' }
    end
  end

end
