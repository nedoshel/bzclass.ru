require 'will_paginate/array'
class Admin::PayoutsController < AdminController

  def index
    @payouts = Payout.includes(:recepient)
    @payouts = @payouts.with_status(:waiting) if params[:f].blank?
    @payouts = @payouts.with_status(:paid) if params[:f] == 'paid'
    @payouts = @payouts.group_by{ |p| p.recepient_id }
    @payouts_keys = @payouts.keys.paginate(page: params[:page], per_page: params[:per_page] || 20)
  end

  def pay
    user = User.find(params[:user_id])
    user.waiting_payouts.each { |payout| payout.pay! }
    user.account.update is_waiting_for_payout: false
    respond_to do |format|
      format.html { redirect_to admin_payouts_path, notice: 'Успешно выведено' }
    end
  end

end
