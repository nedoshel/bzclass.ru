class Admin::PostsController < AdminController
  defaults resource_class: Post, collection_name: 'posts', instance_name: 'post'
  before_action :load_categories, only: :index

  protected

  def collection
    get_collection_ivar ||
      set_collection_ivar(
        end_of_association_chain
        .includes(:category)
        .paginate page: params[:page])
  end

  def post_params
    params.require(:post)
      .permit(:id, :category_id, :name, :short_description, :content, :image, :is_main)
  end

  def load_categories
    @categories = Category.all
  end
end

