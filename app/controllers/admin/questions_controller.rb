class Admin::QuestionsController < AdminController
  defaults resource_class: Question, collection_name: 'questions', instance_name: 'question'


  protected
    def collection
      get_collection_ivar || set_collection_ivar(end_of_association_chain.paginate(page: params[:page]))
    end

    def question_params
      params.require(:question).permit(:id, :name, :is_active, :answer)
    end

end

