class Admin::SettingsController < AdminController

  def index
    @settings = Setting.unscoped.where.not(var: [:reset_password_email, :hello_email, :success_payment, :resembling_email])
  end

  def edit
    @setting = Setting.unscoped.find params[:id]
  end

  def update
    setting = Setting.unscoped.find params[:id]
    respond_to do |format|
      Setting[setting.var] = params[:setting][:value]
      format.html { redirect_to admin_settings_path, notice: 'Успешное редактирование' }
    end
  end

end
