class Admin::UsersController < AdminController

  def index
    @users = User.includes(:account)
    @users = @users.with_status(:register) if params[:f] == 'register'
    @users = @users.with_status(:register_and_paid) if params[:f] == 'paid'
    @users = @users.paginate(page: params[:page], per_page: 30)
  end

  def show

  end

  def new
    @user = User.new
    @user.parent_id = params[:id]
  end

  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        login(params[:user][:email], params[:user][:password])
        format.html { redirect_to login_path, notice: 'Вы успешно зарегистрировались в системе' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      update_params = user_params.slice(:email, :password, :password_confirmation)
      update_params = update_params.slice(:email) if update_params[:password].blank?
      if current_user.update(update_params)
        format.html { redirect_to profile_path, notice: 'Успешное редактирование' }
      else
        format.html { render :show }
      end
    end
  end

  private
    def user_params
      params.require(:user).permit(
        :name,
        :phone,
        :email,
        :password,
        :password_confirmation,
        :parent_id
      )
    end
end
