class AdminController < InheritedResources::Base

  before_action :authenticate
  skip_before_action :require_login, :redirect_if_do_not_pay
  self.responder = ApplicationResponder
  respond_to :html
  protect_from_forgery with: :exception
  # http_basic_authenticate_with name: lambda { Setting.admin_login }, password: lambda { Setting.admin_password }
  layout 'admin'

  def authenticate
    authenticate_or_request_with_http_basic('Administration') do |username, password|
      username == Setting.admin_login && password == Setting.admin_password
    end
  end
end
