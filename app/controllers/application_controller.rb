require "application_responder"

class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception
  before_action :require_login, :set_mailer_host, :redirect_if_do_not_pay

  def current_user_path(user=current_user)
    user.is_active? ? profile_path : new_payment_path
  end

  def not_authenticated
    flash[:warning] = 'Необходимо войти или зарегистрироваться.'
    redirect_to login_path
  end

  def faq
    @questions = Question.is_active
  end

  def grant
  end

  def grant_request
    GrantMailer.new_request(params).deliver_now
    redirect_to profile_path, notice: 'Ваша заявка отправлена и будет рассмотрена.'
  end

  private

    def set_mailer_host
      ActionMailer::Base.default_url_options[:host] = request.host_with_port
    end

    def redirect_if_do_not_pay
      redirect_to new_payment_path unless current_user.is_active?
    end

end
