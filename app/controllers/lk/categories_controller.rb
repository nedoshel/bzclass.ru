class Lk::CategoriesController < ApplicationController

  def index
    @categories = Category.all
    @category = @categories.first
    @main_post = @category.posts.is_main.first
    @posts = @category.posts.not_main.paginate(page: params[:page], per_page: 3).order(id: :desc)
  end

  def show
    @categories = Category.all
    @category = Category.includes(:posts).find_by_slug(params[:slug])
    @main_post = @category.posts.is_main.first
    @posts = @category.posts.not_main.paginate(page: params[:page], per_page: 3).order(id: :desc)
  end

end
