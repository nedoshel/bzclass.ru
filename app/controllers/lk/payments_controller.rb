class Lk::PaymentsController < ApplicationController
  skip_before_action :redirect_if_do_not_pay

  def new
    redirect_to lk_news_path if current_user.is_active? && current_user.days_left > 10
    @payment = current_user.current_payment
  end

end
