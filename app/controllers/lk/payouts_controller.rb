class Lk::PayoutsController < ApplicationController
  def new
    @payout = current_user.payouts.new
    @payouts = current_user.payouts.includes(:refferal)
  end

  def create
    if current_user.account.is_waiting_for_payout?
      redirect_to new_payout_path, alert: 'Вы уже ожидаете вывода средств!'
    elsif params[:payout][:wallet].blank?
      redirect_to new_payout_path, alert: 'Укажите номер qiwi кошелька для вывода'
    else
      current_user.create_payouts(params[:payout][:wallet])
      redirect_to new_payout_path, notice: 'Ваш запрос в ближайшее время будет рассмотрен.'
    end
  end

end
