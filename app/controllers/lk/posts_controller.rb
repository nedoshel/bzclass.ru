class Lk::PostsController < ApplicationController

  def index
  end

  def show
    @category = Category.find_by_slug params[:category_slug]
    @post = @category.posts.find_by_slug params[:slug]
  end

end
