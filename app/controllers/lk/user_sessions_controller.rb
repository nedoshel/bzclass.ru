class Lk::UserSessionsController < ApplicationController

  skip_before_action :require_login, except: [:destroy]
  skip_before_action :redirect_if_do_not_pay
  layout 'login'

  def new
    redirect_to current_user_path if current_user
    @user = User.new
  end

  def create
    if @user = login(params[:email], params[:password], params[:remember_me])
      redirect_to current_user_path(@user), notice: 'Успешный вход!'
    else
      flash.now[:alert] = 'Проблемы авторизации'
      render action: 'new'
    end
  end

  def destroy
    logout
    # redirect_to root_path, notice: 'Успешный выход!'
    redirect_to root_path
  end
end
