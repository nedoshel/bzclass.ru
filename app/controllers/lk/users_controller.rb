class Lk::UsersController < ApplicationController
  skip_before_action :require_login, :redirect_if_do_not_pay, only: [ :new, :create ]
  layout 'login', only: [ :new, :create ]
  before_action :redirect_to_profile, only: [ :new, :create ]
  def show

  end

  def new
    @user = User.new
    @user.parent_id = params[:id]

  end

  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        login(params[:user][:email], params[:user][:password])
        format.html { redirect_to login_path, notice: 'Вы успешно зарегистрировались в системе' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if User.authenticate(current_user.email, params[:user][:current_password])
        update_params = user_params.slice(:password, :password_confirmation)
        if current_user.update(update_params)
          format.html { redirect_to profile_path, notice: 'Успешное редактирование' }
        else
          format.html { render :show }
        end
      else
        flash.now[:alert] = 'Текущий пароль некорректен'
        format.html { render :show }
      end
    end
  end

  private
    def user_params
      params.require(:user).permit(
        :name,
        :phone,
        :email,
        :password,
        :password_confirmation,
        :parent_id,
        :acceptance_of_the_offer
      )
    end

    def redirect_to_profile
      redirect_to profile_path if current_user
    end
end
