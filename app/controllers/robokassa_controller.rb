class RobokassaController < ApplicationController

  skip_before_action :require_login, :set_mailer_host, :redirect_if_do_not_pay

  # "OutSum"=>"2000.0", "InvId"=>"3", "SignatureValue"=>"8C7CFCA6CCF5EBFDE699BA0207FF842C", "path"=>"robokassa/paid"
  def paid
    signature = Digest::MD5.hexdigest("#{params[:OutSum]}:#{params[:InvId]}:#{Rubykassa.second_password}").upcase
    if params[:SignatureValue] == signature
      payment = Payment.find(params[:InvId])
      payment.pay!
      render text: "OK#{params[:InvId]}"
    else
      render text: "Error#{params[:InvId]}"
    end
  end

  def success
    payment = Payment.find(params[:InvId])
    redirect_to profile_path, notice: "Ваш платеж на сумму #{payment.amount} руб. будет принят в ближайшее время. Спасибо!"
  end

  def fail
    redirect_to new_payment_path, alert: "Во время принятия платежа возникла ошибка. Мы скоро разберемся!"
  end

end
