class WelcomeController < ActionController::Base
  protect_from_forgery with: :exception
  layout 'welcome'

  def index
    @category = Category.is_public.first
  end

  def about
  end

  def contacts
  end

  def feedback
    FeedbackMailer.send_feedback(params.slice(:name, :email, :context)).deliver_later
    redirect_to contacts_path
  end

  def news
    @category = Category.is_public.first
  end

  def show
    @category = Category.find_by_slug params[:category_slug]
    @post = @category.posts.find_by_slug params[:slug]
  end

end
