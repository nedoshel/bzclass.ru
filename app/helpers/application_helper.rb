module ApplicationHelper
  def main_menu_li_class(*paths)
    paths.each do |path|
      return 'act active' if current_page?(path) || lk_news_path?(path)
    end
  end

  def amount_in_currency(amount)
    "#{amount.to_i} <i class='fa fa-1x fa-rub'></i>".html_safe
  end

  def amount_without_percent(amount, percent = 18)
    amount - (amount * percent / 100)
  end

  private

  def lk_news_path?(path)
    path == lk_news_path && ['categories', 'posts'].include?(controller_name)
  end
end
