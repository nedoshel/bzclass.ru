class ApplicationMailer < ActionMailer::Base
  default from: "postmaster@bzclass.ru"
  layout 'mailer'
end
