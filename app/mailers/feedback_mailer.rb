# info page feedback
class FeedbackMailer < ApplicationMailer
  def send_feedback(args)
    @name = args[:name]
    @email = args[:email]
    @context = args[:context]
    mail(
      reply_to: @email, to: Setting.feedback_email,
      subject: 'Новый фидбэк на сайте bzclass.ru',
      content_type: 'text/html'
    )
  end
end
