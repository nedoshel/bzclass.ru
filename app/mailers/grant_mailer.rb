class GrantMailer < ApplicationMailer
  def new_request(params)
    @params = params
    if Rails.env.development?
      if params[:file].present?
        file = params[:file].read
        attachments[params[:file].original_filename] = {
          mime_type: params[:file].content_type,
          content: file
        }
      end
      mail(to: Setting.grant_email, subject: "Заявка на ГРАНТ на сайте bzclass.ru", content_type: "text/html", reply_to: @params[:email])
    else
      mg_client = Mailgun::Client.new 'key-1692d90022239557acaddf618cdc6992'
      text = render_to_string(template: "grant_mailer/new_request", layout: false)
      message_params = {
        from: 'postmaster@bzclass.ru',
        to: Setting.grant_email,
        subject: 'Заявка на ГРАНТ на сайте bzclass.ru',
        html: text.to_str,
        'h:Reply-To' => @params[:email],
        attachment: params[:file],
        multipart: true
      }
      mg_client.send_message 'bzclass.ru', message_params
    end
  end
end
