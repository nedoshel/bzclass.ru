class UserMailer < ApplicationMailer

  def reset_password_email(user)
    @user = user
    @url  = edit_password_reset_url(user.reset_password_token)
    @content = Setting.reset_password_email
    mail(to: user.email, subject: "Восстановление пароля на портале bzclass.ru", content_type: "text/html")
  end

  def hello_email(user)
    @payment_url = new_payment_url
    @user = user
    @content = Setting.hello_email
    mail(to: user.email, subject: "Ваш аккаунт на портале bzclass.ru активирован", content_type: "text/html")
  end

  def resembling_email(user)
    @payment_url = new_payment_url
    @user = user
    @content = Setting.resembling_email
    mail(to: user.email, subject: "Ваша подписка на портале bzclass.ru скоро закончится", content_type: "text/html")
  end

  def success_payment(user)
    @user = user
    @content = Setting.success_payment
    mail(to: user.email, subject: "Вы успешно оплатили подписку на портале bzclass.ru", content_type: "text/html")
  end
end
