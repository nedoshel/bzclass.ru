class Category < ActiveRecord::Base
  has_many :posts, dependent: :destroy, counter_cache: true
  validates :name, presence: true
  # validates :is_public, presence: true
  include Sluggable

  scope :is_public, -> { where(is_public: true) }

end
