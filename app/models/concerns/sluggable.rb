module Sluggable
  extend ActiveSupport::Concern

  included do

    after_create :set_slug
    before_update :update_slug

  end

  def set_slug
    self.update slug: pretty_slug
  end

  def update_slug
    if name_changed? || slug.blank?
      self.slug = pretty_slug
    else
      return
    end
  end

  def pretty_slug
    "#{id}-#{Russian.translit(name).gsub(/\s/, '_').gsub(/\W/,'').gsub('_', '-')}"
  end

end
