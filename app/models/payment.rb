# user_id
# is_paid
# amount

class Payment < ActiveRecord::Base

  DEFAULT_AMOUNT = 3000

  belongs_to :user

  scope :unpaid, -> { where(payments: { is_paid: false }) }
  scope :paid,   -> { where(payments: { is_paid: true }) }
  scope :active, -> { where(":time_now < payments.updated_at", time_now: Time.current - 1.year ) }
  scope :less_that_10, -> { where %Q(
    (TO_DAYS(DATE_ADD(payments.updated_at, INTERVAL 1 YEAR)) - TO_DAYS(CURRENT_DATE())) < 12
    )
  }

  before_create :set_amount
  after_update :send_success_message

  def pay!
    return if is_paid
    ActiveRecord::Base.transaction do
      update is_paid: true
      user.update status: :register_and_paid
      if user.parent.present?
        user.parent.account.update amount: (user.parent.account.amount + amount)
      end
    end
  end

  def status
    is_paid? ? 'Оплачено' : 'Ожидает оплаты'
  end

  def days_left
    last_day = updated_at + 1.year
    seconds_left = last_day - Time.current
    days = seconds_left / 60 / 60 / 24
    days.to_i
  end

  private
    def set_amount
      self.amount ||= DEFAULT_AMOUNT
    end

    def send_success_message
      return unless is_paid_changed?
      return if is_paid == false
      UserMailer.success_payment(user).deliver_later
    end

end
