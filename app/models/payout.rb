# recepient_id
# refferal_id
# wallet
# amount
# status

class Payout < ActiveRecord::Base
  extend Enumerize

  belongs_to :recepient, class_name: User
  belongs_to :refferal, class_name: User

  enumerize :status, in: { waiting: 1, paid: 2 }, default: 1, scope: :with_status
  validates :wallet, presence: true

  after_update :decrement_recepient_account_amount

  def pay!
    update status: :paid
  end

  private
    # уменьшает баланс получателя, если статус изменился на "оплачен"
    def decrement_recepient_account_amount
      return if !status_changed? && status != 'paid'
      recepient.account.update amount: recepient.account.amount - amount
    end
end
