class Post < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  belongs_to :category

  validates :category_id, presence: true
  validates :name, presence: true
  validates :content, presence: true

  include Sluggable

  scope :is_main, -> { where(is_main: true) }
  scope :not_main, -> { where(is_main: false) }

end
