class Question < ActiveRecord::Base
  scope :is_active, -> { where(is_active: true) }
end
