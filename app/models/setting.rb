# Admin mailers controller
class Setting < RailsSettings::CachedSettings
  scope :mailers, -> {
    where(var: [:reset_password_email, :hello_email, :success_payment, :resembling_email])
  }

end
