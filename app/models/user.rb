  class User < ActiveRecord::Base
  authenticates_with_sorcery!
  extend Enumerize
  attr_accessor :acceptance_of_the_offer
  belongs_to :parent,    class_name: User, foreign_key: :parent_id, primary_key: :id
  has_many   :payouts,   foreign_key: :recepient_id, primary_key: :id, dependent: :destroy
  has_many   :payments,  dependent: :destroy
  has_many   :childrens, class_name: User, foreign_key: :parent_id, primary_key: :id,  dependent: :nullify

  has_one :account, class_name: UserAccount

  # === Validations ====
  validates :password,                presence: true, confirmation: true, length: { minimum: 6 }, if: :password
  validates :password_confirmation,   presence: true, if: '!password.nil?'
  validates :phone, uniqueness: true, presence: true
  validates :email, uniqueness: true, presence: true
  validates :name, presence: true
  validate :acceptance_with_the_offer, on: :create

  def acceptance_with_the_offer
    return if acceptance_of_the_offer.to_i == 1
    errors.add(:acceptance_of_the_offer, 'необходимо согласиться')
  end
  # === End validations ===
  scope :paid_childrens, -> { childrens.includes(:payments).where(payments: { is_paid: true }) }

  after_create :send_hello_message
  after_create :create_user_account

  enumerize :status, in: { register: 1, register_and_paid: 2, not_extended: 3 }, default: 1, scope: :with_status

  def self.send_resembling_messages
    User.includes(:payments).merge(Payment.active.paid.less_that_10).each do |user|
      UserMailer.resembling_email(user).deliver_later
    end
  end

  def send_reset_password_email!
    UserMailer.reset_password_email(self).deliver_later
  end

  def is_active?
    payments.paid.active.any?
  end

  def current_payment
    payments.unpaid.first_or_create
  end

  def current_bill
    account.amount
  end

  # def status
  #   is_active? ? 'Зарегистрировался и оплатил' : 'Зарегистрировался'
  # end

  def days_left
    payments.active.paid.last.days_left
  end

  def childrens_payments
    payout_ids = payouts.pluck(:refferal_id)
    payments = Payment.paid.where(user: childrens)
    payments = payments.where.not(payments: { user_id: payout_ids }) if payout_ids.any?
    payments_count = payments.count
    payments_count.even? ? payments : payments.limit(payments_count - 1)
  end

  # Создать выплаты
  def create_payouts(wallet)
    ActiveRecord::Base.transaction do
      account.update is_waiting_for_payout: true
      childrens_payments.each do |children_payment|
        payouts.create(refferal_id: children_payment.user_id,
                        amount: children_payment.amount,
                        wallet: wallet
                        )
      end
    end
  end

  def available
    amount = account.amount - waiting_amount
    index = (amount / Payment::DEFAULT_AMOUNT).to_i
    index -= 1 if index.odd?
    Payment::DEFAULT_AMOUNT * index
  end

  # ожидаемые выплаты
  def waiting_payouts
    payouts.with_status(:waiting)
  end

  # Сумма, ожидаемая к выводу
  def waiting_amount
    waiting_payouts.sum(:amount)
  end

  private

    def send_hello_message
      UserMailer.hello_email(self).deliver_later
    end

    def create_user_account
      create_account
    end
end
