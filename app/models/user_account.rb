# user_id
# amount
# is_waiting_for_payout
class UserAccount < ActiveRecord::Base
  belongs_to :user

  def available
    index = (amount / Payment::DEFAULT_AMOUNT).to_i
    index -= 1 if index.odd?
    Payment::DEFAULT_AMOUNT * index
  end
end
