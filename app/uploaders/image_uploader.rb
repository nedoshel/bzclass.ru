class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :thumb do
    # process resize_and_pad: [150, 150, '#FFFFFF']
    # process :auto_orient
    # process resize_to_fill: [240, 240]
    process resize_and_pad: [240, 240, '#FFFFFF']
  end

  version :_1000 do
    process resize_to_fit: [1000, nil]
  end

  def auto_orient
    manipulate! do |image|
      image.tap(&:auto_orient)
    end
  end

end
