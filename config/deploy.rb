# config valid only for current version of Capistrano
lock '3.4.0'

set :repo_url, 'git@bitbucket.org:nedoshel/bzclass.ru.git'

set :rvm_type, :user
set :rvm_ruby_version, '2.2.2'

set :linked_files, %w{config/database.yml config/secrets.yml log/sidekiq.log}

# set :linked_dirs, %w{log tmp/pids tmp/sockets vendor/bundle public/uploads}
set :linked_dirs, %w{log tmp/pids tmp/sockets vendor/bundle public/uploads}

set :keep_releases, 2

# ===== sidekiq =====
set :pty,  false
set :sidekiq_pid, -> { "#{shared_path}/tmp/pids/bzclass.ru.sidekiq.pid" }
# set :sidekiq_options_per_process, ["--queue high", "--queue default --queue mailers"]
set :sidekiq_options_per_process, ["--queue default --queue mailers"]

# namespace :fonts do
#   desc "ln assets/fonts to public"
#   task :create do
#     on roles(:app) do
#       within current_path do
#         execute "ln -nfs #{current_path}/app/assets/fonts #{current_path}/public/fonts"
#       end
#     end
#   end
#   after 'deploy:finishing', 'fonts:create'
# end

namespace :deploy do
  desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end
  # thin

  commands = %w(start stop restart)
  commands.each do |command|
    desc "#{command} Thin"
    task command do
      on roles(:app) do
        within current_path do
          # if fetch(:stage) == :production
          #   execute "service office-my-kolibri-com.httpd #{command}"
          # else
          execute :bundle, "exec thin #{command} -C #{fetch(:thin_config)}"
          # end
        end
      end
    end
  end

  # RVM integration
  if Gem::Specification::find_all_by_name('capistrano-rvm').any?
    commands.each { |c| before c, 'rvm:hook' }
  end

  after 'deploy:publishing', 'deploy:restart'

end

# namespace :websocket_rails do
#   desc 'Stop websocket deamon'
#   task :stop_deamons do
#     on roles(:app), in: :sequence, wait: 5 do
#       within current_path do
#         websocket_pid = current_path.join('tmp/pids/websocket_rails.pid')
#         if test("[ -f #{websocket_pid} ]") then
#           with rails_env: fetch(:stage) do
#             rake 'websocket_rails:stop_server'
#           end
#         else
#           info "WebsocketRails is not running, not need to be stopped."
#         end
#       end
#     end
#   end

#   desc 'Start websocket deamon'
#   task :start_deamons do
#     on roles(:app), in: :sequence, wait: 5 do
#       within current_path do
#         with rails_env: fetch(:stage) do
#           info 'Start WebsocketRails server'
#           rake 'websocket_rails:start_server'
#         end
#       end
#     end
#   end

#   after 'deploy:restart', 'websocket_rails:stop_deamons'
#   after 'websocket_rails:stop_deamons', 'websocket_rails:start_deamons'
# end

# def warn_if_standalone_not_enabled!
#   return if WebsocketRails.standalone?
#   puts "Fail!"
#   puts "You must enable standalone mode in your websocket_rails.rb initializer to use the standalone server."
#   exit 1
# end
