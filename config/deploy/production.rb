set :stage, :production

set :ssh_options, { forward_agent: true, port: 22 }
set :branch, 'master'
set :application, "bzclass.ru"
set :deploy_to, "/home/deploy/bzclass.ru"
set :thin_config, "config/thin/#{fetch(:application)}.yml"

server '178.62.179.17', user: 'deploy', roles: %w{web app db}
