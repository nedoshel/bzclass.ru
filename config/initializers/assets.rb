Rails.application.config.assets.version = '1.0'

Rails.application.config.assets.paths << "#{Rails.root}/app/assets/fonts"
# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

Rails.application.config.assets.precompile += %w( ckeditor/* )
Rails.application.config.assets.precompile += %w( welcome.css welcome.js )
Rails.application.config.assets.precompile += %w( admin.css admin.js )
Rails.application.config.assets.precompile += %w( bootstrap_and_overrides.css )
Rails.application.config.assets.precompile += %w( lk.js )
