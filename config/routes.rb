Rails.application.routes.draw do

  require 'sidekiq/web'
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == 'bzclass' && password == '12345'
  end
  mount Sidekiq::Web, at: "/sidekiq"
  mount Ckeditor::Engine => '/ckeditor'

  controller 'welcome' do
    get 'index'
    get 'news'
    get 'contacts'
    post 'feedback'
    get 'about'
    get 'news/:category_slug/:slug' => 'welcome#show',            as: :post
  end

  root 'welcome#index'



  scope 'lk', module: 'lk' do
    root to: redirect('/login'),   as: :lk
    resources     :users, except: [ :index, :destroy ]
    resources     :user_sessions
    resources     :password_resets
    resources     :payments
    resources     :payouts
    get 'news'                      => 'categories#index',      as: :lk_news
    get 'news/:slug'                => 'categories#show',       as: :lk_category
    get 'news/:category_slug/:slug' => 'posts#show',            as: :lk_post
  end

  namespace 'admin', path: 'adm' do
    root 'users#index'
    resources :users
    resources :questions
    resources :payouts, only: [ :index ]
    post 'payouts/:user_id/pay' => 'payouts#pay', as: :pay
    resources :settings
    resources :mailers
    resources :posts
    resources :categories
  end

  get 'login'     =>  'lk/user_sessions#new',     as: :login
  get 'logout'    =>  'lk/user_sessions#destroy', as: :logout
  get 'profile'   =>  'lk/users#show',            as: :profile
  get 'lk/faq'    => 'application#faq',           as: :faq
  get 'lk/grant'  => 'application#grant',         as: :grant
  post 'lk/grant' => 'application#grant_request', as: :grant_request
end
