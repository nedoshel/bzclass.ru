class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :user, index: true, foreign_key: true
      t.decimal :amount, precision: 10,  scale: 2
      t.boolean :is_paid, default: false

      t.timestamps null: false
    end
  end
end
