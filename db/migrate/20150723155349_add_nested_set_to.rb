class AddNestedSetTo < ActiveRecord::Migration
  def change
    add_column :users, :parent_id, :integer, :null => true, :index => true, after: :name
  end
end
