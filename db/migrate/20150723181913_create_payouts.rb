class CreatePayouts < ActiveRecord::Migration
  def change
    create_table :payouts do |t|
      t.references :recepient, index: true
      t.references :refferal, index: true
      t.decimal :amount, precision: 10,  scale: 2
      t.integer :status, default: 0

      t.timestamps null: false
    end
    add_foreign_key :payouts, :users, column: :recepient_id
    add_foreign_key :payouts, :users, column: :refferal_id
  end
end
