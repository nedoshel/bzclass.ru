class CreateUserAccounts < ActiveRecord::Migration
  def change
    create_table :user_accounts do |t|
      t.references :user, index: true, foreign_key: true
      t.decimal :amount, precision: 10,  scale: 2, default: 0.0

      t.timestamps null: false
    end
  end
end
