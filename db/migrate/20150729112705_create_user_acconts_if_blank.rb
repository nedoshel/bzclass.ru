class CreateUserAccontsIfBlank < ActiveRecord::Migration
  def change
    User.eager_load(:account)
      .where(user_accounts: { id: nil })
      .find_each do |user|
        amount = Payment.paid.includes(:user).where(user: user.childrens).sum(:amount)
        user.create_account(amount: amount)
    end
  end
end
