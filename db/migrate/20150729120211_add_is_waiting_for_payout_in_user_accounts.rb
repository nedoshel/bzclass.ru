class AddIsWaitingForPayoutInUserAccounts < ActiveRecord::Migration
  def change
    add_column :user_accounts, :is_waiting_for_payout, :boolean, default: false, after: :amount
  end
end
