class AddStatusToUsers < ActiveRecord::Migration
  def change
    add_column :users, :status, :integer
    User.all.each do |user|
      user.update(status: :register_and_paid) if user.is_active?
    end
  end
end
