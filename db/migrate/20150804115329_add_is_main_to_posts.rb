class AddIsMainToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :is_main, :boolean, default: false
  end
end
