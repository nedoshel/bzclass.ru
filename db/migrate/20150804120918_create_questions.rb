class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :name
      t.text :answer
      t.boolean :is_active, default: true

      t.timestamps null: false
    end
  end
end
