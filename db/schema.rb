# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150806090735) do

  create_table "authentications", force: :cascade do |t|
    t.integer  "user_id",    limit: 4,   null: false
    t.string   "provider",   limit: 255, null: false
    t.string   "uid",        limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "authentications", ["provider", "uid"], name: "index_authentications_on_provider_and_uid", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.boolean  "is_public"
    t.integer  "posts_count", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "slug",        limit: 255
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",    limit: 255, null: false
    t.string   "data_content_type", limit: 255
    t.integer  "data_file_size",    limit: 4
    t.integer  "assetable_id",      limit: 4
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width",             limit: 4
    t.integer  "height",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",           limit: 255, null: false
    t.integer  "sluggable_id",   limit: 4,   null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope",          limit: 255
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "payments", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.decimal  "amount",               precision: 10, scale: 2
    t.boolean  "is_paid",                                       default: false
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
  end

  add_index "payments", ["user_id"], name: "index_payments_on_user_id", using: :btree

  create_table "payouts", force: :cascade do |t|
    t.integer  "recepient_id", limit: 4
    t.integer  "refferal_id",  limit: 4
    t.string   "wallet",       limit: 255
    t.decimal  "amount",                   precision: 10, scale: 2
    t.integer  "status",       limit: 4,                            default: 0
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
  end

  add_index "payouts", ["recepient_id"], name: "index_payouts_on_recepient_id", using: :btree
  add_index "payouts", ["refferal_id"], name: "index_payouts_on_refferal_id", using: :btree

  create_table "posts", force: :cascade do |t|
    t.integer  "category_id",       limit: 4
    t.string   "name",              limit: 255
    t.string   "short_description", limit: 255
    t.text     "content",           limit: 65535
    t.string   "image",             limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "slug",              limit: 255
    t.boolean  "is_main",                         default: false
  end

  add_index "posts", ["category_id"], name: "index_posts_on_category_id", using: :btree

  create_table "questions", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.text     "answer",     limit: 65535
    t.boolean  "is_active",                default: true
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "settings", force: :cascade do |t|
    t.string   "var",        limit: 255,   null: false
    t.text     "value",      limit: 65535
    t.integer  "thing_id",   limit: 4
    t.string   "thing_type", limit: 30
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "settings", ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true, using: :btree

  create_table "user_accounts", force: :cascade do |t|
    t.integer  "user_id",               limit: 4
    t.decimal  "amount",                          precision: 10, scale: 2, default: 0.0
    t.boolean  "is_waiting_for_payout",                                    default: false
    t.datetime "created_at",                                                               null: false
    t.datetime "updated_at",                                                               null: false
  end

  add_index "user_accounts", ["user_id"], name: "index_user_accounts_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                           limit: 255,             null: false
    t.string   "phone",                           limit: 255
    t.string   "name",                            limit: 255
    t.integer  "parent_id",                       limit: 4
    t.string   "crypted_password",                limit: 255
    t.string   "salt",                            limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remember_me_token",               limit: 255
    t.datetime "remember_me_token_expires_at"
    t.string   "reset_password_token",            limit: 255
    t.datetime "reset_password_token_expires_at"
    t.datetime "reset_password_email_sent_at"
    t.integer  "failed_logins_count",             limit: 4,   default: 0
    t.datetime "lock_expires_at"
    t.string   "unlock_token",                    limit: 255
    t.integer  "status",                          limit: 4
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["remember_me_token"], name: "index_users_on_remember_me_token", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", using: :btree

  add_foreign_key "payments", "users"
  add_foreign_key "payouts", "users", column: "recepient_id"
  add_foreign_key "payouts", "users", column: "refferal_id"
  add_foreign_key "posts", "categories"
  add_foreign_key "user_accounts", "users"
end
